
const tabJs = document.querySelectorAll('.tab-js'),
rightTabJs = document.querySelectorAll('.right-tab-js'),
leftJs = document.querySelector('.left-js'),
partJs = document.querySelectorAll('.part-js'),
textPartJs = document.querySelectorAll('.text-part-js'),
btnJsGo = document.querySelector('.btn-js-go'),
btnJsGo1 = document.querySelector('.btn-js-go-1'),
tabsPart = document.querySelectorAll('.tabs-part a'),
tabsPartRow = document.querySelector('.tabs-part'),
EVORICHPartnershipRow = document.querySelector('.EVORICH-partnership-row'),
EVORICHPartnershipRowBorder = document.querySelectorAll('.EVORICH-partnership-row-border');




let count = 0;
let countTop = 0;

tabsPartRow.addEventListener('click', (e) => {
  e.preventDefault();

  if (e.target.closest('.tabs-part a')) {

    [...tabsPart].map((elem, index) => {
      elem.addEventListener('click', () => {
        countTop = index;
      });
      elem.classList.remove('active');

      EVORICHPartnershipRowBorder.forEach(function (item) {
        item.classList.remove('active');
         if (item.dataset.id === e.target.dataset.id) {
           e.target.classList.add('active');
           item.classList.add('active');
         }
      });
    });
  }

})
leftJs.addEventListener('click', (e) => {
  e.preventDefault();

  if (e.target.closest('.tab-js')) {

    [...tabJs].map((elem, index) => {
      elem.addEventListener('click', () => {
        countTop = index;
      });
      elem.classList.remove('active');

      rightTabJs.forEach(function (item) {
        item.classList.remove('activeTab');
         if (item.dataset.tabOpen === e.target.dataset.tab) {
           e.target.classList.add('active');
           item.classList.add('activeTab');
         }
      });
    });
  }

})




const sliderGo = (event) => {
  event.preventDefault();

  if (textPartJs.length - 1 === count) {
      count = 0;
  } else {
      count += 1;
  }

  for (let i = 0; i < textPartJs.length; i++) {
    textPartJs[i].classList.remove('activeTabs');

    textPartJs[count].classList.add('activeTabs');
  
  }
  for (let i = 0; i < partJs.length; i++) {
    partJs[i].classList.remove('active');

    partJs[count].classList.add('active');
  
  }
};


const sliderGoTop = (event) => {
  event.preventDefault();

  if (rightTabJs.length - 1 === countTop) {
    countTop = 0;
  } else {
    countTop += 1;
  }

  for (let i = 0; i < rightTabJs.length; i++) {
    rightTabJs[i].classList.remove('activeTab');

    rightTabJs[countTop].classList.add('activeTab');
  
  }
  for (let i = 0; i < tabJs.length; i++) {
    tabJs[i].classList.remove('active');

    tabJs[countTop].classList.add('active');
  
  }
};


// счетчик подчета 

// var Animation = function() {
//   var hFrom = $(window).scrollTop();
//   var hTo = $(window).scrollTop() + ($(window).height() / 3);
//   if ($(".int-fact-counters").offset().top > hFrom && $(".int-fact-counters").offset().top < hTo){
//       $('.fct-count span b').delay(500).spincrement({
//           thousandSeparator: "",
//           duration: 3000,
//       });
//   }
// };
// $(window).scroll(function() {
//   Animation();
// });
// Animation();


$(document).ready(function(){

  function countup(className){
    var countBlockTop = $(className).offset().top;
    var windowHeight = window.innerHeight;
    var show = true;
          
    $(window).scroll( function (){
      if(show && (countBlockTop < $(window).scrollTop() + windowHeight)){ 
        show = false;
            
        $(className).delay(500).spincrement({
          //from: 1,
          duration: 3000,
          thousandSeparator: " ",
        });
        console.log('fire');
      }
    })  
  }

    countup(".count", $(".count").text());
    countup(".count2", $(".count2").text());
    countup(".count3", $(".count3").text());


});



btnJsGo1.addEventListener('click', sliderGoTop);


