const header = document.querySelector('.header'),
langStart = document.querySelectorAll('.lang'),
btnWileJs = document.querySelector('.btn-wile-js'),
bgImagesInfo = document.querySelector('.bg-images-info'),
bg50 = document.querySelector('.bg-50'),
$menuDropMobile = document.querySelector('.menu-drop-mobile'),
$menuDropMobileSpan = document.querySelector('.menu-drop-mobile span'),
$menuDropMobileVisible = document.querySelector('.menu-drop-mobile-visible'),
$modalMenuDropHeader = document.querySelector('.modal-menu-drop-header'),
$liHover = document.querySelector('.li-hover');

window.onscroll = () => {
    const scrl = window.scrollY;
    if (scrl > 10) {
      header.classList.add('active');
    }
    if (scrl < 10) {
      header.classList.remove('active');
    }
};

langStart.forEach(item => {

  item.addEventListener('click', (e) => {
    e.preventDefault();
    let langDrop = item.querySelector('.lang-drop');
    setTimeout(() => {
      langDrop.classList.toggle('active');
    }, 50);

  });

});

$menuDropMobile.addEventListener('click', e => {
  if (e.target.closest('span')) {
    $menuDropMobile.classList.toggle('active');
  }

  
});

  
document.querySelectorAll('a[href^="#"').forEach(link => {

  link.addEventListener('click', function(e) {
      e.preventDefault();

      let href = this.getAttribute('href').substring(1);

      const scrollTarget = document.getElementById(href);

      const topOffset = document.querySelector('.scrollto').offsetHeight;
      // const topOffset = 0; // если не нужен отступ сверху 
      const elementPosition = scrollTarget.getBoundingClientRect().top;
      const offsetPosition = elementPosition - topOffset;

      window.scrollBy({
          top: offsetPosition,
          behavior: 'smooth'
      });
  });
});

if ($liHover) {
  $liHover.addEventListener('mousemove', () => {
    $liHover.classList.add('active');
    $modalMenuDropHeader.classList.add('active');
  });
  $modalMenuDropHeader.addEventListener('mouseleave', () => {
    $liHover.classList.remove('active');
    $modalMenuDropHeader.classList.remove('active');
  });
}